<?
// user_ip_address
if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) $user_ip_address=$_SERVER['HTTP_X_FORWARDED_FOR'];
else $user_ip_address=$_SERVER['REMOTE_ADDR'];
// uid
$uid = $modx->user->get('id');
// query
$query = "SELECT ip1, ip2, ip3 FROM modx_savalo_ipz WHERE uid = " . $uid;

$result = $modx->query($query);
$row = $result->fetch(PDO::FETCH_NUM);

$ip1 = $row[0];
$ip2 = $row[1];
$ip3 = $row[2];

$zalogovat = 'NE';
if ($ip1 == '') {
	// ulozeni IP adresy 1
	$query = "UPDATE modx_savalo_ipz SET ip1='$user_ip_address' WHERE uid=$uid";
	$result = $modx->query($query);
	$zalogovat = 'ANO';
} else {
	// neni prazdna, tak kontrola jestli neni stejna
	if ($ip1 == $user_ip_address) {
		$zalogovat = 'ANO';
	} else {
		// ulozeni IP adresy 2
		if ($ip2 == '') {
			// prazdne
			$query = "UPDATE modx_savalo_ipz SET ip2='$user_ip_address' WHERE uid=$uid";
			$result = $modx->query($query);
			$zalogovat = 'ANO';
		} else {
			// neni prazdna, tak kontrola jestli neni stejna
			if ($ip2 == $user_ip_address) {
				$zalogovat = 'ANO';
			} else {
				// ulozeni IP adresy 3
				if ($ip3 == '') {
					// prazdne
					$query = "UPDATE modx_savalo_ipz SET ip3='$user_ip_address' WHERE uid=$uid";
					$result = $modx->query($query);
					$zalogovat = 'ANO';
				} else {
					// neni prazdna, tak kontrola jestli neni stejna
					if ($ip3 == $user_ip_address) {
						$zalogovat = 'ANO';
					}
				}
			}
		}
	}
}

if ($zalogovat=='NE') {
	$url = $modx->makeUrl(54);
	$modx->sendRedirect($url);
}
?>